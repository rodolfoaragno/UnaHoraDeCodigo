#!./venv/bin/python
from flask import Flask, request, url_for, redirect, render_template
from views import Views

app = Flask( __name__ )
v = Views()
v.menu = { 
"home": "/", 
"tables":"/tables"}


class data1():
	def __init__(self):
		self.name = "undef"
		self.sname = "undef" 
		self.points = -1 
	def __getitem__(self, i):
		return self.__dict__[i]
	def keys(self):
		return self.__dict__.keys()

data_holder = data1

b = data_holder()
b.name = "data holder object"
b.other = "just a bunch o' data"
b.end = "happy"
data = [
{"name": "user", "sname":"local", "points":128}
,b, data_holder()
]

menu = [ 
("home", "/"),
("tables", "/tables"),
("Curriculum","/cv")
  ]

@app.route('/')
def home():
	return render_template("template.html", app_name= "Fwitter", menu = menu )

@app.route('/tables')
def tables():
	return render_template("table.html",
	data = data,
	t_data = data_holder().keys(),
	app_name = "Fwitter",
	menu = menu)


@app.route('/cv')
def cv():
	return render_template("cv.html")

@app.route('/ajax',methods=['GET'])
def ajax():
	return render_template("cv.html")

app.debug=True
app.run(port=5000) 

